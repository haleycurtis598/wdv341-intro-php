<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Jackson Guitars</title>


    	<!-- BOOTSTRAP STYLING, NAVBAR STYLING, CUSTOM STYLESHEET AND GOOGLE FONTS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
      <link rel="stylesheet" href="https://cdn.rawgit.com/balzss/luxbar/ae5835e2/build/luxbar.min.css">
      <link rel="stylesheet" type="text/css" href="style.css">
      <link href='http://fonts.googleapis.com/css?family=Comfortaa' rel='stylesheet' type='text/css'>


      <style type="text/css">

        body {
          background-image: url(images/landingpage.jpg);
        }

        .card {
          width: 100%;
          height: 100%;
          display: block;
          margin-top: 20px;
          border: black 1px solid;
        }

        #rotateAdmin {
          -ms-transform: rotate(-25deg); /* IE 9 */
          -webkit-transform: rotate(-25deg); /* Safari 3-8 */
          transform: rotate(-25deg);
        }

        #rotateCustomer {
          -ms-transform: rotate(25deg); /* IE 9 */
          -webkit-transform: rotate(25deg); /* Safari 3-8 */
          transform: rotate(25deg);
        }

        #rotateAdmin:hover {
          -ms-transform: rotate(0deg); /* IE 9 */
          -webkit-transform: rotate(0deg); /* Safari 3-8 */
          transform: rotate(0deg);
        }

        #rotateCustomer:hover {
          -ms-transform: rotate(0deg); /* IE 9 */
          -webkit-transform: rotate(0deg); /* Safari 3-8 */
          transform: rotate(0deg);
        }


      </style>

  </head>
  <body>

    <!-- <h2>Welcome to</h2> -->

    <img src="images/jacksonlogo.png" alt="guitar" width="40%" height="30%" style="margin-top:0.9%">


  <div class="container">
  <div class="row">
    <div class="col-md">
    <div class="card">
        <div class="card-body">
          <h4 class="card-title">Admin Login</h4>
          <a href="loginAdmin.php"><img class="card-img-top" id="rotateCustomer" src="images/guitarAdmin.png" alt="Card image cap"></a>
          <p class="card-text"></p>
          <br>
          <a href="loginAdmin.php"><input id="button" type="button" value="Go" class="btn btn-default"></a>
        </div>
    </div>
  </div>


  <div class="col-md">
    <div class="card">
        <div class="card-body">
          <h4 class="card-title">Customer Login</h4>
          <a href="loginCustomer.php"><img class="card-img-top" id="rotateAdmin" src="images/guitarCustomer.png" alt="Card image cap"></a>
          <p class="card-text"></p>
          <br>
          <a href="loginCustomer.php"><input id="button" type="button" value="Go" class="btn btn-default"></a>
        </div>
    </div>
    </div>
  </div>
</div>
















    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

  </body>
</html>
