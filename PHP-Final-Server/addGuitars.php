<html>
<head>
<title>Jackson Guitars</title>
<!-- BOOTSTRAP STYLING, NAVBAR STYLING, CUSTOM STYLESHEET AND GOOGLE FONTS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.rawgit.com/balzss/luxbar/ae5835e2/build/luxbar.min.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link href='http://fonts.googleapis.com/css?family=Comfortaa' rel='stylesheet' type='text/css'>
</head>
<body>

  <header id="luxbar" class="luxbar-fixed">
      <input type="checkbox" class="luxbar-checkbox" id="luxbar-checkbox"/>
      <div class="luxbar-menu luxbar-menu-right luxbar-menu-dark">
          <ul class="luxbar-navigation">
              <li class="luxbar-header">
                  <a href="homeFinalAdmin.php" class="luxbar-brand">Home</a>
                  <label class="luxbar-hamburger luxbar-hamburger-doublespin"
                  id="luxbar-hamburger" for="luxbar-checkbox"> <span></span> </label>
              </li>
              <li class="luxbar-item"><a href="addGuitars.php">Add Guitar(s)</a></li>
              <li class="luxbar-item"><a href="logoutAdmin.php">Logout</a></li>
          </ul>
      </div>
  </header>

<div id="main">
<h2 style="padding-top: 8%;">Add a New Guitar</h2><br />

<form action="addGuitars.php" method="post">
  <label>Name:</label>
    <input type="text" name="guitar_name" id="name" required="required" placeholder="Ex: Z Series MO500"/>

  <label>Price:</label>
    <input type="text" name="guitar_price" id="price" required="required" placeholder="Ex: 500.00"/>

  <label>Color:</label>
    <input type="text" name="guitar_color" id="color" required="required" placeholder="Black"/>

  <label>Number of Strings:</label>
    <input type="text" name="guitar_number_of_strings" id="number_of_strings" required="required" placeholder="Ex: 6"/>

  <label>Finish:</label>
    <input type="text" name="guitar_finish" id="finish" required="required" placeholder="Ex: Gloss"/>

  <label>Shape:</label>
    <input type="text" name="guitar_shape" id="shape" required="required" placeholder="Juggernaut"/>

  <label>Material:</label>
    <input type="text" name="guitar_material" id="material" required="required" placeholder="Mahogany"/>

<input type="submit" value="Add" name="submit"/><br /><br />
</form>

<h3><a href="homeFinalAdmin.php"><input type="button" value="View All Guitars"></a></h3>

</div>
<?php
if(isset($_POST["submit"])){
$hostname='localhost';
$username='root';
$password='root';

try {
$dbh = new PDO("mysql:host=$hostname;dbname=wdv341",$username,$password);

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // <== add this line
$sql = "INSERT INTO guitar_info (guitar_name, guitar_price, guitar_color, guitar_number_of_strings, guitar_finish, guitar_shape, guitar_material, guitar_img)
VALUES ('".$_POST["guitar_name"]."','".$_POST["guitar_price"]."','".$_POST["guitar_color"]."','".$_POST["guitar_number_of_strings"]."','".$_POST["guitar_finish"]."','".$_POST["guitar_shape"]."','".$_POST["guitar_material"]."','".$_POST["guitar_img"]."')";
if ($dbh->query($sql)) {
echo "<script type= 'text/javascript'>alert('New Record Inserted Successfully!');</script>";
}
else{
echo "<script type= 'text/javascript'>alert('Something went wrong, try again.');</script>";
}

$dbh = null;
}
catch(PDOException $e)
{
echo $e->getMessage();
}

}
?>




<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>
