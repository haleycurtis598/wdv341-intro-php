<?php

session_start();

require 'database.php';

//create sql query
$sql = "SELECT `id`,
`guitar_name`,
`guitar_price`,
`guitar_color`,
`guitar_number_of_strings`,
`guitar_finish`,
`guitar_shape`,
`guitar_material`,
`guitar_img`
FROM `guitar_info`";
//prepare the statement
$statement = $conn->prepare($sql);
//check if preparation was successful
if (!$statement) {
    $message .= "Preparing statement failed";
}
//execute statement
$statement->execute();

if( isset($_SESSION['user_id']) ){

	$records = $conn->prepare('SELECT id,email,password FROM users WHERE id = :id');
	$records->bindParam(':id', $_SESSION['user_id']);
	$records->execute();
	$results = $records->fetch(PDO::FETCH_ASSOC);

	$user = NULL;

	if( count($results) > 0){
		$user = $results;
	}

}


?>
<html>
<head>
	<title>Jackson Guitars</title>

	<!-- BOOTSTRAP STYLING, NAVBAR STYLING, CUSTOM STYLESHEET AND GOOGLE FONTS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdn.rawgit.com/balzss/luxbar/ae5835e2/build/luxbar.min.css">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link href='http://fonts.googleapis.com/css?family=Comfortaa' rel='stylesheet' type='text/css'>

<style>
		.displayGuitarInfo {
				width: 80%;
				margin: 10px auto;
				border: solid 1px #c6c6c6;
        padding: 15px;
        text-align: center;
        height: auto;
		}
		p {
				text-align: center;
		}

		h1, h2, h3 {
				text-align: center;
		}
		h1 {
				color: #628588;
		}

    .description {
      font-size: 1.5em;
    }

    h4 {
      text-align: center;
      font-weight: bold;
    }
    .title {
      font-size: 1.5em;
    }

		.label {
				color: black;
        font-weight: normal;
        font-size: 1em;
		}

		img {
			margin-top: 7%;
		}
</style>


</head>
<body>

		<center><img src="images/jacksonlogo.png" alt="logo" height="30%" width="40%"></center>

    <header id="luxbar" class="luxbar-fixed">
        <input type="checkbox" class="luxbar-checkbox" id="luxbar-checkbox"/>
        <div class="luxbar-menu luxbar-menu-right luxbar-menu-dark">
            <ul class="luxbar-navigation">
                <li class="luxbar-header">
                    <a href="homeFinalAdmin.php" class="luxbar-brand">Home</a>
                    <label class="luxbar-hamburger luxbar-hamburger-doublespin"
                    id="luxbar-hamburger" for="luxbar-checkbox"> <span></span> </label>
                </li>
                <li class="luxbar-item"><a href="addGuitars.php">Add Guitar(s)</a></li>
                <li class="luxbar-item"><a href="logoutAdmin.php">Logout</a></li>
            </ul>
        </div>
    </header>

    <br /><h3><strong>Welcome <?= $user['email']; ?></strong></h3>
		You are successfully logged in! <br /><br />

    <h3> <?php echo $statement->rowCount(); ?> guitars available.</h3>

		<?php
		//Display each row as formatted output
		while ($row = $statement->fetch()) //Turn each row of the result into an associative array
		{
		    //For each row you have in the array create a block of formatted text
		    ?>

		    <div class="displayGuitarInfo">
		            <h4 class="title"></span> <?php echo $row['guitar_name']; ?></h4>
            <p class="displayGuitarImage">
		            <span class="label"></span><img src="images/<?php echo $row['guitar_img']; ?>" alt="guitar" height="40%" width="80%">
		        </p>
            <h4 class="description">Description:</h4>
		        <p class="displayGuitarPrice">
		            <span class="label">Price:</span> $ <?php echo $row['guitar_price']; ?>
		        </p>
		        <p class="displayGuitarColor">
		            <span class="label">Color:</span> <?php echo $row['guitar_color']; ?>
		        </p>
		        <p class="displayGuitarStrings">
		            <span class="label">Number of Strings:</span> <?php echo $row['guitar_number_of_strings']; ?>
		        </p>
		        <p class="displayGuitarFinish">
		            <span class="label">Finish:</span> <?php echo $row['guitar_finish']; ?>
		        </p>
						<p class="displayGuitarShape">
		            <span class="label">Shape:</span> <?php echo $row['guitar_shape']; ?>
		        </p>
						<p class="displayGuitarMaterial">
		            <span class="label">Material:</span> <?php echo $row['guitar_material']; ?>
		        </p>

		    </div>

		    <?php
		}//close while loop
		$statement = null;
		$conn = null;    //Close the database connection
		?>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>
