-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Dec 14, 2017 at 06:28 AM
-- Server version: 5.6.35
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `wdv341`
--

-- --------------------------------------------------------

--
-- Table structure for table `guitar_info`
--

CREATE TABLE `guitar_info` (
  `id` int(11) NOT NULL,
  `guitar_name` varchar(200) NOT NULL,
  `guitar_price` varchar(200) NOT NULL,
  `guitar_color` varchar(200) NOT NULL,
  `guitar_number_of_strings` int(11) NOT NULL,
  `guitar_finish` varchar(200) NOT NULL,
  `guitar_shape` varchar(200) NOT NULL,
  `guitar_material` varchar(200) NOT NULL,
  `guitar_img` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `guitar_info`
--

INSERT INTO `guitar_info` (`id`, `guitar_name`, `guitar_price`, `guitar_color`, `guitar_number_of_strings`, `guitar_finish`, `guitar_shape`, `guitar_material`, `guitar_img`) VALUES
(1, 'Limited Edition Misha Mansoor Juggernaut HT 8', '5,000.00', 'Satin Black', 8, 'Satin', 'Juggernaut', 'Alder', 'jacksonguitar.png'),
(2, 'X Series Jackson King V KVXT', '800.00', 'Cherry Burst', 6, 'Gloss', 'King V', 'Mahogany with Flame Maple Top', 'jacksonguitar3.png'),
(3, 'JS Series Kelly JS32T', '400.00', 'Viola Burst', 6, 'Satin', 'Kelly', 'Poplar', 'jacksonguitar4.png'),
(5, 'Pro Series Rhoads RR3', '1,550.00', 'Ivory with Black Pinstripes', 6, 'Gloss', 'Rhoads', 'Mahogany', 'jacksonguitar6.png'),
(6, 'Pro Series Dinky DK7Q HT', '1,200.00', 'Chlorine Burst', 7, 'Gloss', 'Dinky', 'Alder with Quilt Maple Top', 'jacksonguitar7.png'),
(7, 'X Series Soloist SLATXMG3-6', '900.00', 'Copper Pearl', 6, 'Gloss', 'Soloist', 'Bassword', 'jacksonguitar8.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guitar_info`
--
ALTER TABLE `guitar_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guitar_info`
--
ALTER TABLE `guitar_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
