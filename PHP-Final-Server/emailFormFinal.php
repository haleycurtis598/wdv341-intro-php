<?php
	$errName = "";
	$errEmail = "";
	$errMessage = "";
	$errHuman = "";
	$result = "";
	if (isset($_POST["submit"])) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$message = $_POST['message'];
		$human = intval($_POST['human']);
		$from = 'Demo Contact Form';
		$to = 'example@domain.com';
		$subject = 'Message from Contact Demo ';

		$body ="From: $name\n E-Mail: $email\n Message:\n $message";
		// Check if name has been entered
		if (!$_POST['name']) {
			$errName = 'Please enter your name';
		}

		// Check if email has been entered and is valid
		if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			$errEmail = 'Please enter a valid email address';
		}

		//Check if message has been entered
		if (!$_POST['message']) {
			$errMessage = 'Please enter your message';
		}
		//Check if simple anti-bot test is correct
		if ($human !== 4) {
			$errHuman = 'Your anti-spam is incorrect';
		}
	// If there are no errors, show posted data
	if (!$errName && !$errEmail) {
		$result='<div class="alert alert-success">Thank you, your submission was a success!</div>';
		$result = $result . "<div>Name: $name\n</div>";
		$result = $result . "<div>Email: $email\n</div>";
		$result = $result . "<div>Message: $message\n</div>";
	}
	else {
		$result='<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later.</div>';
	}
}
?>

<?php
	$to      = 'haleycurtis598@gmail.com';
	$subject = 'Submission Success';
	$message = "Name: ".$_POST["name"]."";
	$message = "Email: ".$_POST["email"]."";
	$message = "Comment: ".nl2br($_POST["message"])."";
	$headers = 'From: confirmationemail@example.com' . "\r\n" .
		'Reply-To: confirmationemail@example.com' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		mail($to, $subject, $message, $headers);
?>

<?php
	$to      = $email;
	$subject = 'Submission Success';
	$message = "Name: ".$_POST["name"]."";
	$message = "Email: ".$_POST["email"]."";
	$message = "Comment: ".nl2br($_POST["message"])."";
	$headers = 'From: confirmationemail@example.com' . "\r\n" .
		'Reply-To: confirmationemail@example.com' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		mail($to, $subject, $message, $headers);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Contact Us</title>

		<!-- BOOTSTRAP STYLING, NAVBAR STYLING, CUSTOM STYLESHEET AND GOOGLE FONTS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdn.rawgit.com/balzss/luxbar/ae5835e2/build/luxbar.min.css">
		<link rel="stylesheet" type="text/css" href="style.css">
		<link href='http://fonts.googleapis.com/css?family=Comfortaa' rel='stylesheet' type='text/css'>
		<style type="text/css">

			.btn {
				width: 49%;
			}

			input[type="text"] {

				width: 100%

			}

			img {
			  margin-top: 1%;
			}

		</style>

  </head>
  <body>
		<header id="luxbar" class="luxbar-fixed">
        <input type="checkbox" class="luxbar-checkbox" id="luxbar-checkbox"/>
        <div class="luxbar-menu luxbar-menu-right luxbar-menu-dark">
            <ul class="luxbar-navigation">
                <li class="luxbar-header">
                    <a href="homeFinal.php" class="luxbar-brand">Home</a>
                    <label class="luxbar-hamburger luxbar-hamburger-doublespin"
                    id="luxbar-hamburger" for="luxbar-checkbox"> <span></span> </label>
                </li>
                <li class="luxbar-item"><a href="emailFormFinal.php">Contact Us</a></li>
								<li class="luxbar-item"><a href="logoutCustomer.php">Logout</a></li>
            </ul>
        </div>
    </header>
		<br /><br /><br /><br />
		<center><img src="images/jacksonlogo.png" alt="logo" height="35%" width="40%"></center>

		<br />
  	<div class="container">
  		<div class="row">
  			<div class="col-md-6 col-md-offset-3">
  				<h1 class="page-header text-center">Contact Us</h1>
				<form class="form-horizontal" role="form" method="post" action="emailFormFinal.php">
					<!-- <center><?php echo $result;
								echo "Today is " . date("l m/d/Y") . "<br>";
								echo "The time is " . date("h:i:sa"); ?><br>
					</center> -->

					<br><div class="form-group">
						<label for="name" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="name" name="name" placeholder="First & Last Name" value="<?php if(isset($_POST['name'])) echo htmlspecialchars($_POST['name']); ?>">
							<?php echo "<p class='text-danger'>$errName</p>";?>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<input type="email" class="form-control" id="email" name="email" placeholder="example@email.com" value="<?php if(isset($_POST['email'])) echo htmlspecialchars($_POST['email']); ?>">
							<?php echo "<p class='text-danger'>$errEmail</p>";?>
						</div>
					</div>
					<div class="form-group">
						<label for="message" class="col-sm-2 control-label">Message</label>
						<div class="col-sm-10">
							<textarea class="form-control" rows="4" name="message"><?php if(isset($_POST['message'])) echo htmlspecialchars(isset($_POST['message'])); ?></textarea>
							<?php echo "<p class='text-danger'>$errMessage</p>";?>
						</div>
					</div>
					<div class="form-group">
						<label for="human" class="col-sm-2 control-label">1 + 3 = ?</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="human" name="human" placeholder="Your Answer" value="<?php if(isset($_POST['human'])) echo htmlspecialchars($_POST['human']); ?>">
							<?php echo "<p class='text-danger'>$errHuman</p>";?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-10 col-sm-offset-2">
							<input id="submit" name="submit" type="submit" value="Send" class="btn btn-default">
							<input id="reset" type="reset" name="reset" value="Reset" class="btn btn-default">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-10 col-sm-offset-2">

						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
  </body>
</html>
