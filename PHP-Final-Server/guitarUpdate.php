<?php
session_start();
if ($_SESSION['validUser'] == "yes")	//If this is a valid user allow access to this page
{
	include 'database.php';
	if(isset($_POST["submit"]))
	{
		//The form has been submitted and needs to be processed

		//Get the name value pairs from the $_POST variable into PHP variables
		//The example uses variables with the same name as the name atribute from the form
		$guitar_name = $_POST[guitar_name];
		$guitar_price = $_POST[guitar_price];
		$guitar_color = $_POST[guitar_color];
		$guitar_number_of_strings = $_POST[guitar_number_of_strings];
		$guitar_finish = $_POST[guitar_finish];
		$guitar_shape = $_POST[guitar_shape];
		$guitar_material = $_POST[guitar_material];	//from the hidden field of the update form

		//Create the SQL UPDATE query or command
		$sql = "UPDATE guitar_info SET " ;
		$sql .= "guitar_name=?, ";
		$sql .= "guitar_price=?, ";
		$sql .= "guitar_color=?, ";
		$sql .= "guitar_number_of_strings=?, ";
		$sql .= "guitar_finish=?, ";
		$sql .= "guitar_shape=?, ";
		$sql .= "guitar_material=? ";	//NOTE last one does NOT have a comma after it
		$sql .= " WHERE (id='$id')"; //VERY IMPORTANT

		//echo "<h3>$sql</h3>";			//testing

		$query = $connection->prepare($sql);	//Prepare SQL query

		$query->bind_param('ssssss',$guitar_name,$guitar_price,
			$guitar_color,$guitar_number_of_strings,$guitar_finish,$guitar_shape,$guitar_material);

		if ( $query->execute() )
		{
			$message = "<h1>Your record has been successfully UPDATED the database.</h1>";
			// $message .= "<p>Please <a href='homeFinalAdmin.php'>view</a> your records.</p>";
		}
		else
		{
			$message = "<h1>You have encountered a problem.</h1>";
			$message .= "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
		}

	}//end if submitted
	else
	{
		//The form needs to display the fields of the record to the user for changes
		$updateRecId = $_GET['recId'];	//Record Id to be updated
		//$updateRecId = 2;				//Hard code a key for testing purposes

		echo "<h1>updateRecId: $updateRecId</h1>";

		//Finds a specific record in the table
		$sql = "SELECT id,guitar_name,guitar_price,guitar_color,guitar_number_of_strings,guitar_finish,guitar_shape,guitar_material FROM guitar_info WHERE id=?";
		//echo "<p>The SQL Command: $sql </p>"; //For testing purposes as needed.

		$query = $connection->prepare($sql);

		$query->bind_param("i",$updateRecId);

		if( $query->execute() )	//Run Query and Make sure the Query ran correctly
		{
			$query->bind_result($id,$guitar_name,$guitar_price,$guitar_color,$guitar_number_of_strings,$guitar_finish,$guitar_shape,$guitar_material);

			$query->store_result();

			$query->fetch();
		}
		else
		{
			$message = "<h1>You have encountered a problem with your update.</h1>";
			$message .= "<h2>" . mysqli_error($connection) . "</h2>" ;
		}

	}//end else submitted
}//end Valid User True
else
{
	//Invalid User attempting to access this page. Send person to Login Page
	header('Location: loginAdmin.php');
}
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP - Presenters Admin Example</title>
</head>

<body>

<?php
//If the user submitted the form the changes have been made
if(isset($_POST["submit"]))
{
	echo $message;	//contains a Success or Failure output content
}//end if submitted

else
{	//The page needs to display the form and associated data to the user for changes
?>
<form id="presentersForm" name="presentersForm" method="post" action="guitarUpdate.php">
  <p>Update the following Presenter's Information.  Place the new information in the appropriate field(s)</p>
  <p>First Name:
    <input type="text" name="guitar_name" id="guitar_name"
    	value="<?php echo $guitar_name; ?>"/>	<!-- PHP will put the name into the value of field-->
  </p>
  <p>Last Name:
    <input type="text" name="guitar_price" id="guitar_price"
    	value="<?php echo $guitar_price; ?>" />
  </p>
  <p>City:
    <input type="text" name="guitar_color" id="guitar_color"
       	value="<?php echo $guitar_color; ?>" />
  </p>
  <p>State:
    <input type="text" name="guitar_number_of_strings" id="guitar_number_of_strings"
        value="<?php echo $guitar_number_of_strings; ?>" />
  </p>
  <p>Zip Code:
    <input type="text" name="guitar_finish" id="guitar_finish"
    	value="<?php echo $guitar_finish; ?>" />
  </p>
	<p>Zip Code:
		<input type="text" name="guitar_shape" id="guitar_shape"
			value="<?php echo $guitar_shape; ?>" />
	</p>
  <p>Email Address:
    <input type="text" name="guitar_material" id="guitar_material"
        value="<?php echo $guitar_material; ?>" />
  </p>

  	<!--The hidden form contains the record if for this record.
    	You can use this hidden field to pass the value of record id
        to the update page.  It will go as one of the name value
        pairs from the form.
    -->
  	<input type="hidden" name="id" id="id"
    	value="<?php echo $id ?>" />

  <p>
    <input type="submit" name="submit" id="submit" value="Update" />
    <input type="reset" name="button2" id="button2" value="Clear Form" />
  </p>
</form>

<?php
}//end else submitted
$query->close();
$connection->close();
?>
<p>Return to <a href="loginAdmin.php">Administrator Options</a></p>
</body>
</html>
