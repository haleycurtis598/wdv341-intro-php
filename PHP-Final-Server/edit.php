<?php
// including the database connection file
include_once("classes/Crud.php");

$crud = new Crud();

//getting id from url
$id = $crud->escape_string($_GET['id']);

//selecting data associated with this particular id
$result = $crud->getData("SELECT * FROM guitar_info WHERE id=$id");

foreach ($result as $res) {
	$guitar_name = $res['guitar_name'];
	$guitar_price = $res['guitar_price'];
	$guitar_color = $res['guitar_color'];
	$guitar_number_of_strings = $res['guitar_number_of_strings'];
	$guitar_finish = $res['guitar_finish'];
	$guitar_shape = $res['guitar_shape'];
	$guitar_material = $res['guitar_material'];
}
?>
<html>
<head>
	<title>Edit Guitar</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdn.rawgit.com/balzss/luxbar/ae5835e2/build/luxbar.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link href='http://fonts.googleapis.com/css?family=Comfortaa' rel='stylesheet' type='text/css'>
</head>

<body>

	<header id="luxbar" class="luxbar-fixed">
			<input type="checkbox" class="luxbar-checkbox" id="luxbar-checkbox"/>
			<div class="luxbar-menu luxbar-menu-right luxbar-menu-dark">
					<ul class="luxbar-navigation">
							<li class="luxbar-header">
									<a href="AdminGuitars.php" class="luxbar-brand">Home</a>
									<label class="luxbar-hamburger luxbar-hamburger-doublespin"
									id="luxbar-hamburger" for="luxbar-checkbox"> <span></span> </label>
							</li>
							<li class="luxbar-item"><a href="add.html">Add Guitar(s)</a></li>
							<li class="luxbar-item"><a href="AdminGuitars.php">View All Records</a></li>
							<li class="luxbar-item"><a href="logoutAdmin.php">Logout</a></li>
					</ul>
			</div>
	</header>

	<br/><br /><br />

	<center><img src="images/jacksonlogo.png" alt="logo" height="30%" width="40%"></center>

	<form name="form1" method="post" action="editaction.php">

			<br /><h2>Edit Guitar</h2>

				<label>Name</label>
				<input type="text" name="guitar_name" value="<?php echo $guitar_name;?>">

				<label>Price</label>
				<input type="text" name="guitar_price" value="<?php echo $guitar_price;?>">

				<label>Color</label>
				<input type="text" name="guitar_color" value="<?php echo $guitar_color;?>">

				<label>Number of Strings</label>
				<input type="text" name="guitar_number_of_strings" value="<?php echo $guitar_number_of_strings;?>">

				<label>Finish</label>
				<input type="text" name="guitar_finish" value="<?php echo $guitar_finish;?>">

				<label>Shape</label>
				<input type="text" name="guitar_shape" value="<?php echo $guitar_shape;?>">

				<label>Material</label>
				<input type="text" name="guitar_material" value="<?php echo $guitar_material;?>">

				<td><input type="hidden" name="id" value=<?php echo $_GET['id'];?>></td>
				<td><input type="submit" name="update" value="Update"></td>
			</tr>
		</table>
	</form>

		<a href="AdminGuitars.php"><input type="button" value="View All Records"></a>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>
