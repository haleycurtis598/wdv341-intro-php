<html>
<head>
	<title>Add Data</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdn.rawgit.com/balzss/luxbar/ae5835e2/build/luxbar.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link href='http://fonts.googleapis.com/css?family=Comfortaa' rel='stylesheet' type='text/css'>
</head>

<body>
<?php
//including the database connection file
include_once("classes/Crud.php");
include_once("classes/Validation.php");

$crud = new Crud();
$validation = new Validation();

if(isset($_POST['Submit'])) {
	$guitar_name = $crud->escape_string($_POST['guitar_name']);
	$guitar_price = $crud->escape_string($_POST['guitar_price']);
	$guitar_color = $crud->escape_string($_POST['guitar_color']);
	$guitar_number_of_strings = $crud->escape_string($_POST['guitar_number_of_strings']);
	$guitar_finish = $crud->escape_string($_POST['guitar_finish']);
	$guitar_shape = $crud->escape_string($_POST['guitar_shape']);
	$guitar_material = $crud->escape_string($_POST['guitar_material']);

	$msg = $validation->check_empty($_POST, array('guitar_name','guitar_price','guitar_color', 'guitar_number_of_strings', 'guitar_finish','guitar_shape','guitar_material'));
	$check_strings = $validation->is_strings_valid($_POST['guitar_number_of_strings']);

	// checking empty fields
	if($msg != null) {
		echo $msg;
		//link to the previous page
		echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
	} elseif (!$check_strings) {
		echo 'Please provide proper number of strings.';
	}
	else {
		// if all the fields are filled (not empty)

		//insert data to database
		$result = $crud->execute("INSERT INTO guitar_info(guitar_name,guitar_price, guitar_color,guitar_number_of_strings,guitar_finish,guitar_shape,guitar_material) VALUES('$guitar_name','$guitar_price','$guitar_color','$guitar_number_of_strings','$guitar_finish','$guitar_shape','$guitar_material')");

		//display success message
		echo "<font color='green'>Data added successfully.";
		echo "<br/><a href='AdminGuitars.php'>View Result</a>";
	}
}
?>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>
