<?php
class Validation
{
	public function check_empty($data, $fields)
	{
		$msg = null;
		foreach ($fields as $value) {
			if (empty($data[$value])) {
				$msg .= "$value field empty <br />";
			}
		}
		return $msg;
	}

	public function is_strings_valid($guitar_number_of_strings)
	{
		//if (is_numeric($strings)) {
		if (preg_match("/^[0-9]+$/", $guitar_number_of_strings)) {
			return true;
		}
		return false;
	}

}
?>
