<?php
//including the database connection file
include_once("classes/Crud.php");

$crud = new Crud();

//fetching data in ascending order
$query = "SELECT * FROM guitar_info ORDER BY id ASC";
$result = $crud->getData($query);
//echo '<pre>'; print_r($result); exit;
?>

<html>
<head>
	<title>All Guitars</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdn.rawgit.com/balzss/luxbar/ae5835e2/build/luxbar.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link href='http://fonts.googleapis.com/css?family=Comfortaa' rel='stylesheet' type='text/css'>
	<style type="text/css">

		table {
			text-align: center;
			margin: 0 auto;
			border: 1px #e0e0e0 solid;
		}

		td {
			padding: 10px;
		}

		td img{
    display: block;
    margin-left: auto;
    margin-right: auto;
		width: 80%;
		height: auto;
}
	</style>
</head>

<body>


	<header id="luxbar" class="luxbar-fixed">
			<input type="checkbox" class="luxbar-checkbox" id="luxbar-checkbox"/>
			<div class="luxbar-menu luxbar-menu-right luxbar-menu-dark">
					<ul class="luxbar-navigation">
							<li class="luxbar-header">
									<a href="AdminGuitars.php" class="luxbar-brand">Home</a>
									<label class="luxbar-hamburger luxbar-hamburger-doublespin"
									id="luxbar-hamburger" for="luxbar-checkbox"> <span></span> </label>
							</li>
							<li class="luxbar-item"><a href="add.html">Add Guitar(s)</a></li>
							<li class="luxbar-item"><a href="AdminGuitars.php">View All Guitars</a></li>
							<li class="luxbar-item"><a href="logoutAdmin.php">Logout</a></li>
					</ul>
			</div>
	</header>

<br />
<br />
<br />

<center><img src="images/jacksonlogo.png" alt="logo" height="30%" width="40%"></center>

<br />

	<table width='98%' border=1>

	<tr bgcolor='#ffd968'>
		<td>Name</td>
		<td>Price</td>
		<td>Color</td>
		<td>Number of Strings</td>
		<td>Finish</td>
		<td>Shape</td>
		<td>Material</td>
		<td>Image</td>
		<td>Update</td>
		<td>Delete</td>
	</tr>
	<?php
	foreach ($result as $key => $res) {
	//while($res = mysqli_fetch_array($result)) {
		echo "<tr>";
		echo "<td>".$res['guitar_name']."</td>";
		echo "<td>".$res['guitar_price']."</td>";
		echo "<td>".$res['guitar_color']."</td>";
		echo "<td>".$res['guitar_number_of_strings']."</td>";
		echo "<td>".$res['guitar_finish']."</td>";
		echo "<td>".$res['guitar_shape']."</td>";
		echo "<td>".$res['guitar_material']."</td>";
		echo "<td><img src='images/".$res['guitar_img']."'></td>";
		echo "<td><a href=\"edit.php?id=$res[id]\">Edit</a></td>";
		echo "<td><a href=\"delete.php?id=$res[id]\" onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a></td>";
	}
	?>
</table><br />

	<a href="add.html"><input type="button" name="data" value="Add New Guitar"></a><br/><br/>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>
