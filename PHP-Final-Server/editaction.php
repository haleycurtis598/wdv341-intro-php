<?php
// including the database connection file
include_once("classes/Crud.php");
include_once("classes/Validation.php");

$crud = new Crud();
$validation = new Validation();

if(isset($_POST['update']))
{
	$id = $crud->escape_string($_POST['id']);

	$guitar_name = $crud->escape_string($_POST['guitar_name']);
	$guitar_price = $crud->escape_string($_POST['guitar_price']);
	$guitar_color = $crud->escape_string($_POST['guitar_color']);
	$guitar_number_of_strings = $crud->escape_string($_POST['guitar_number_of_strings']);
	$guitar_finish = $crud->escape_string($_POST['guitar_finish']);
	$guitar_shape = $crud->escape_string($_POST['guitar_shape']);
	$guitar_material = $crud->escape_string($_POST['guitar_material']);

	$msg = $validation->check_empty($_POST, array('guitar_name','guitar_price','guitar_color', 'guitar_number_of_strings','guitar_finish','guitar_shape','guitar_material'));
	$check_strings = $validation->is_strings_valid($_POST['guitar_number_of_strings']);


	// checking empty fields
	if($msg) {
		echo $msg;
		//link to the previous page
		echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
	} elseif (!$check_strings) {
		echo 'Please provide proper number of strings.';
	} else {
		//updating the table
		$result = $crud->execute("UPDATE guitar_info SET guitar_name='$guitar_name',guitar_price='$guitar_price',guitar_color='$guitar_color',guitar_number_of_strings='$guitar_number_of_strings',guitar_finish='$guitar_finish',guitar_shape='$guitar_shape',guitar_material='$guitar_material' WHERE id=$id");

		//redirectig to the display page. In our case, it is index.php
		header("Location: AdminGuitars.php");
	}
}
?>
