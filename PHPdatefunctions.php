<?php

  try {
      $dateMDY = new DateTime('2017-09-16');
  }
  catch (Exception $e) {
      echo $e->getMessage();
      exit(1);
  }

  try {
      $dateDMY = new DateTime('2017-09-16');
  }
  catch (Exception $e) {
      echo $e->getMessage();
      exit(1);
  }

  $str = " DMACC ";

  $var1 = "DMACC";
  $var2 = "dmacc";
  if (strcasecmp($var1, $var2) == 0) {
      //echo '$var1 is equal to $var2 in a case-insensitive string comparison';
  }

  $number = 123456;

  $number2 = 12344567890;

 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>PHP Date Functions</title>
  </head>
  <body>

  <h1>WDV341 PHP Functions</h1>

    <p>mm/dd/yyyy: <?php echo $dateMDY->format('m/d/Y'); ?></p>

    <p>dd/mm/yyyy: <?php echo $dateDMY->format('d/m/Y'); ?></p>

    <p>Lowercase: <?php echo $str = strtolower($str); ?></p>

    <p>Display Number of Characters in String: <?php echo strlen($str); ?></p>

    <p>Trim Whitespace: <?php echo $str = trim($str); ?></p>

    <p>String Comparison: <?php echo "$var1 and $var2 are equal in a case-insensitive string comparison"; ?></p>

    <p>Format Number into Thousands: <?php echo $english_format_number = number_format($number2); ?></p>

    <p>Format Number into Currency: <?php setlocale(LC_MONETARY, 'en_US'); echo money_format('%i', $number) . "\n"; ?></p>


  </body>
</html>
