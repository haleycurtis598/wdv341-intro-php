<?php

$servername = "localhost";
$username = "haleycur_dae";
$password = "wdv341";
$dbname = "haleycur_wdv341_event";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

	if (isset($_POST["submit"])) {
		$contact_name = $_POST['contact_name'];
		$contact_email = $_POST['contact_email'];
		$contact_reason = $_POST['contact_reason'];
		$contact_comments = $_POST['contact_comments'];
		$contact_newsletter = $_POST['contact_newsletter'];
		$contact_more_products = $_POST['contact_more_products'];
		$contact_date = $_POST['contact_date'];
		$contact_time = $_POST['contact_time'];
		$human = intval($_POST['human']);
		$from = 'Contact Form';
		$to = 'example@email.com';
		$subject = 'Message from Contact Demo ';


		$body ="From: $name\n E-Mail: $email\n Message:\n $message";
		// Check if name has been entered
		if (!$_POST['contact_name']) {
			$errName = 'Please enter your name';
		}

		// Check if email has been entered and is valid
		if (!$_POST['contact_email'] || !filter_var($_POST['contact_email'], FILTER_VALIDATE_EMAIL)) {
			$errEmail = 'Please enter a valid email address';
		}

		//Check if message has been entered
		if (!$_POST['contact_reason']) {
			$errReason = 'Please enter your reason';
		}

		if (!$_POST['contact_comments']) {
			$errComments = 'Please enter your comments';
		}

		if (!$_POST['contact_newsletter']) {
			$errNewsletter = 'Please enter your newsletter';
		}

		if (!$_POST['contact_more_products']) {
			$errProducts = 'Please enter your products';
		}

		if (!$_POST['contact_date']) {
			$errDate = 'Please enter your date';
		}

		if (!$_POST['contact_time']) {
			$errTime = 'Please enter your time';
		}
		//Check if simple anti-bot test is correct
		if ($human !== 5) {
			$errHuman = 'Your anti-spam is incorrect';
		}

		$sql = "INSERT INTO wdv341_customer_contacts (contact_name, contact_email, contact_reason, contact_comments, contact_newsletter, contact_more_products, contact_date, contact_time)
		VALUES ('$contact_name', '$contact_email', '$contact_reason', '$contact_comments', '$contact_newsletter', '$contact_more_products', '$contact_date', '$contact_time')";

		if ($conn->query($sql) === TRUE) {
				$message = "New record created successfully";
		} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
		}
// If there are no errors, send the email
if (!$errName && !$errEmail && !$errReason && !$errComments && !$errNewsletter && !$errProducts && !$errDate && !$errTime && !$errHuman) {
	if (mail ($to, $subject, $body, $from)) {
		$result='<div class="alert alert-success">Thank You, your submission was a success!</div>';
	} else {
		$result='<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later.</div>';
	}
}
	}


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP Contact Form</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<style media="screen">
			#submit {
				width: 49.5%;
			}
			#reset {
				width: 49.5%;
			}
		</style>
  </head>
  <body>
  	<div class="container">
  		<div class="row">
  			<div class="col-md-6 col-md-offset-3">
  				<h1 class="page-header text-center">PHP Contact Form</h1>
				<form class="form-horizontal" role="form" method="post" action="contactFormDatabaseUpdate.php">
					<?php echo $result; ?>
					<div class="form-group">
						<label for="name" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="First & Last Name" value="<?php echo htmlspecialchars($_POST['contact_name']); ?>">
							<?php echo "<p class='text-danger'>$errName</p>";?>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<input type="email" class="form-control" id="contact_email" name="contact_email" placeholder="example@email.com" value="<?php echo htmlspecialchars($_POST['contact_email']); ?>">
							<?php echo "<p class='text-danger'>$errEmail</p>";?>
						</div>
					</div>
					<div class="form-group">
						<label for="message" class="col-sm-2 control-label">Reason</label>
						<div class="col-sm-10">
							<textarea class="form-control" rows="4" id="contact_reason" name="contact_reason"><?php echo htmlspecialchars($_POST['contact_reason']);?></textarea>
							<?php echo "<p class='text-danger'>$errReason</p>";?>
						</div>
					</div>
					<div class="form-group">
						<label for="optgroup" class="col-sm-2 control-label">Comments</label>
						<div class="col-sm-10">
							<textarea class="form-control" rows="4" id="contact_comments" name="contact_comments"><?php echo htmlspecialchars($_POST['contact_comments']);?></textarea>
							<?php echo "<p class='text-danger'>$errComments</p>";?>
						</div>
					</div>

					<div class="form-group">
						<label for="name" class="col-sm-2 control-label">Newsletter</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="contact_newsletter" name="contact_newsletter" value="<?php echo htmlspecialchars($_POST['contact_newsletter']); ?>">
							<?php echo "<p class='text-danger'>$errNewsletter</p>";?>
						</div>
					</div>

					<div class="form-group">
						<label for="optgroup" class="col-sm-2 control-label">More Products</label>
						<div class="col-sm-10">
							<textarea class="form-control" rows="4" id="contact_more_products" name="contact_more_products"><?php echo htmlspecialchars($_POST['contact_more_products']);?></textarea>
							<?php echo "<p class='text-danger'>$errProducts</p>";?>
						</div>
					</div>

					<div class="form-group">
						<label for="name" class="col-sm-2 control-label">Date</label>
						<div class="col-sm-10">
							<input type="date" class="form-control" id="contact_date" name="contact_date" value="<?php echo htmlspecialchars($_POST['contact_date']); ?>">
							<?php echo "<p class='text-danger'>$errDate</p>";?>
						</div>
					</div>

					<div class="form-group">
						<label for="name" class="col-sm-2 control-label">Time</label>
						<div class="col-sm-10">
							<input type="time" class="form-control" id="contact_time" name="contact_time" value="<?php echo htmlspecialchars($_POST['contact_time']); ?>">
							<?php echo "<p class='text-danger'>$errTime</p>";?>
						</div>
					</div>

					<div class="form-group">
						<label for="human" class="col-sm-2 control-label">2 + 3 = ?</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="human" name="human" placeholder="Your Answer">
							<?php echo "<p class='text-danger'>$errHuman</p>";?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-10 col-sm-offset-2">
							<input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
							<input id="reset" type="reset" name="reset" value="Reset" class="btn btn-default">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-10 col-sm-offset-2">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
  </body>
</html>
