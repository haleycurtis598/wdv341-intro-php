<?php
$servername = "localhost";
$username = "haleycur_dae";
$password = "wdv341";
$dbname = "haleycur_wdv341_event";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

	$event_name = $_POST['event_name'];
	$event_location = $_POST['event_location'];
	$event_date = $_POST['event_date'];


$sql = "INSERT INTO wdv341_event (event_name, event_location, event_date)
VALUES ('$event_name', '$event_location', '$event_date')";

if ($conn->query($sql) === TRUE) {
    $message = "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();

?>


<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Events Form</title>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <style>

      #container {
        width: 40%;
				border: solid black 1px;
				padding: 30px;
      }

    </style>

  </head>
  <body>

    <center>
    <h3>Register an Event</h3>


    <div id="container">

    <form action="eventsFormBrowser.php" method="post">

			<div class="form-group">
      		<label for="event_name">Event Name:</label>
        	<input class="form-control" type="text" name="event_name" id="event_name">
			</div>
<br>
			<div class="form-group">
      		<label for="event_location">Event Location:</label>
        	<input class="form-control" type="text" name="event_location" id="event_location">
			</div>
<br>
			<div class="form-group">
      		<label for="event_date">Event Date:</label>
        	<input class="form-control" type="date" name="event_date" id="event_date">
			</div>
<br>

			<?php echo $message; ?><br><br>

      <input type="submit" name="submit" value="Submit" class="btn btn-primary">
      <input type="reset" name="reset" value="Reset" class="btn btn-default">

    </form>

  </div>
</center>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

  </body>
</html>
